The API Guide
=============

.. toctree::
    :hidden:

    introduction
    channels
    orders
    checkouts
    products
    users
    shipments
    payments
    stock_locations

.. include:: /api/map.rst.inc
